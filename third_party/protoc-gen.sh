#!/usr/bin/env bash
protoc --proto_path=gcchr.api/proto/v1 --proto_path=gcchr.api/third_party --go_out=plugins=grpc:gcchr.api/pkg/api/v1 user-service.proto
